# SQL Project

## Description
First we created a small superhero database using scripts and no UI to see basic behavior of a SQL database.\
Then we used a database from Noroff Academy and created some manipulation methods for this database(Chinook).


## Table of Contents

- [Getting started](#getting-started)
- [Installation](#installation)
- [Usage](#usage)
- [Support](#support)
- [Authors and acknowledgment](#authors-and-acknowledgment)
- [Project status](#project-status)


## Getting started

1. Clone the repo
2. Open solution in Visual Studio
3. You need the Chinook DB in SSMS
4. Change the DB source in the ConnectionStringHelper class => connectionStringBuilder.DataSource = @"YourSQLServerName" 
5. Uncomment the desired method in Main(Program.cs) and run to see the result

For SuperheroDB run the queries in the zip folder.


## Installation
Make sure you have installed at least the following tools:
```
• Visual Studio 2019
• .NET 5
• NuGet Package "Microsoft.Data.SqlClient"
• SQL Server Management Studio
```

## Usage
This project is for learning purposes only.


## Support
You can get support from the Authors of this repo


## Authors and acknowledgment
@angelova24 and @Petar-Programing-Petrov


## License
For open source project.


## Project status
Works as expected and taking in mind the assignment 6 expectations and requirements