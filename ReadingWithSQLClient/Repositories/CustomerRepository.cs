﻿using Microsoft.Data.SqlClient;
using ReadingWithSQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingWithSQLClient.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer currentCustomer = new Customer();
                                currentCustomer.CustomerId = reader.GetInt32(0);
                                currentCustomer.FirstName = reader.GetString(1);
                                currentCustomer.LastName = reader.GetString(2);
                                currentCustomer.Country = reader[3] as string;
                                currentCustomer.PostalCode = reader[4] as string;
                                currentCustomer.Phone = reader[5] as string;
                                currentCustomer.Email = reader[6] as string;

                                customers.Add(currentCustomer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customers;
        }

        public Customer GetCustomerById(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer\nWHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader[3] as string;
                                customer.PostalCode = reader[4] as string;
                                customer.Phone = reader[5] as string;
                                customer.Email = reader[6] as string;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        public Customer GetCustomerByName(string name)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer\nWHERE FirstName LIKE @FirstName";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", name);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader[3] as string;
                                customer.PostalCode = reader[4] as string;
                                customer.Phone = reader[5] as string;
                                customer.Email = reader[6] as string;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }
        
        public List<Customer> GetLimitCustomers(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer\nORDER BY CustomerId\nOFFSET @offset ROWS\nFETCH NEXT @limit ROWS ONLY";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@limit", limit);
                        cmd.Parameters.AddWithValue("@offset", offset);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer currentCustomer = new Customer();
                                currentCustomer.CustomerId = reader.GetInt32(0);
                                currentCustomer.FirstName = reader.GetString(1);
                                currentCustomer.LastName = reader.GetString(2);
                                currentCustomer.Country = reader[3] as string;
                                currentCustomer.PostalCode = reader[4] as string;
                                currentCustomer.Phone = reader[5] as string;
                                currentCustomer.Email = reader[6] as string;

                                customers.Add(currentCustomer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customers;
        }
        
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email)\nVALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //cmd.Parameters.AddWithValue("@CustomerId", customer);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }
    
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer\n SET FirstName = @FirstName, " +
                "LastName = @LastName, Country = @Country, PostalCode = @PostalCode, " +
                "Phone = @Phone, Email = @Email\nWHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return success;
        }
        
        public List<CustomerCountry> GetCountriesWithNumberOfCustomers()
        {
            List<CustomerCountry> stats = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(*) AS CustomerCount\nFROM Customer\nGROUP BY Country\nORDER BY CustomerCount DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry currentStat = new CustomerCountry();
                                currentStat.Country = reader.GetString(0);
                                currentStat.CustomerCount = reader.GetInt32(1);

                                stats.Add(currentStat);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return stats;
        }
        
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> highestSpenders = new List<CustomerSpender>();
            string sql = "SELECT c.CustomerId, c.FirstName, c.LastName, SUM(i.Total) AS Total FROM Invoice i\n" +
                "JOIN Customer c ON i.CustomerId = c.CustomerId\n" +
                "GROUP BY c.CustomerId, c.FirstName, c.LastName\n" +
                "ORDER BY Total DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender currentSpender = new CustomerSpender();
                                currentSpender.CustomerId = reader.GetInt32(0);
                                currentSpender.FirstName = reader.GetString(1);
                                currentSpender.LastName = reader.GetString(2);
                                currentSpender.TotalSum = reader.GetDecimal(3);

                                highestSpenders.Add(currentSpender);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return highestSpenders;
        }
        
        public List<CustomerGenre> GetMostPopularGenreOfACustomer(int id)
        {
            List<CustomerGenre> genres = new List<CustomerGenre>();
            string sql = "SELECT TOP 1 WITH TIES i.CustomerId, c.FirstName, c.LastName, g.Name AS Genre, COUNT(*) AS Played FROM Invoice i\n" +
                "JOIN Customer c ON i.CustomerId = c.CustomerId\n" +
                "JOIN InvoiceLine il ON i.InvoiceId = il.InvoiceId\n" +
                "JOIN Track t ON il.TrackId = t.TrackId\n" +
                "JOIN Genre g ON t.GenreId = g.GenreId\n" +
                "WHERE i.CustomerId = @CustomerId\n" +
                "GROUP BY g.Name, i.CustomerId, c.FirstName, c.LastName\n" +
                "ORDER BY Played DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre genre = new CustomerGenre();
                                genre.CustomerId = reader.GetInt32(0);
                                genre.FirstName = reader.GetString(1);
                                genre.LastName = reader.GetString(2);
                                genre.Genre = reader.GetString(3);
                                genre.PlayedCount = reader.GetInt32(4);

                                genres.Add(genre);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return genres;
        }
    }
}
