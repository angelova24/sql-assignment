﻿using ReadingWithSQLClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingWithSQLClient.Repositories
{
    public interface ICustomerRepository
    {
        /// <summary>
        /// Gets all customers from DB
        /// </summary>
        /// <returns>List with all customers</returns>
        public List<Customer> GetAllCustomers();
        /// <summary>
        /// Gets a customer by id
        /// </summary>
        /// <param name="id">id of a customer</param>
        /// <returns>Object Customer</returns>
        public Customer GetCustomerById(int id);
        /// <summary>
        /// Gets a customer by first name
        /// </summary>
        /// <param name="name">first name of a customer</param>
        /// <returns>Object Customer</returns>
        public Customer GetCustomerByName(string name);
        /// <summary>
        /// Gets a subset of customers from DB
        /// </summary>
        /// <param name="limit">the count of customers to get</param>
        /// <param name="offset">the count of customers to skip before return</param>
        /// <returns>List with the desired amount of customers</returns>
        public List<Customer> GetLimitCustomers(int limit, int offset);
        /// <summary>
        /// Adds a new customer to DB
        /// </summary>
        /// <param name="customer">the new customer object</param>
        /// <returns>True if successful or false if not</returns>
        public bool AddNewCustomer(Customer customer);
        /// <summary>
        /// Updates an existing customer from DB based on the customer id
        /// </summary>
        /// <param name="customer">customer object</param>
        /// <returns>True if successful or false if not</returns>
        public bool UpdateCustomer(Customer customer);
        /// <summary>
        /// Gets the number of customers in each country
        /// </summary>
        /// <returns>List of countries with number of customers, ordered descending</returns>
        public List<CustomerCountry> GetCountriesWithNumberOfCustomers();
        /// <summary>
        /// Gets customers who are the highest spenders
        /// </summary>
        /// <returns>List of Customers, ordered descending</returns>
        public List<CustomerSpender> GetHighestSpenders();
        /// <summary>
        /// Gets the most popular genre of a customer based on customer id
        /// </summary>
        /// <param name="id">id of a customer</param>
        /// <returns>List with the most popular genre (in case of a tie, both)</returns>
        public List<CustomerGenre> GetMostPopularGenreOfACustomer(int id);
    }
}