﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingWithSQLClient.Repositories
{
    public class ConnectionStringHelper
    {
        /// <summary>
        /// This method establishes the connection
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {
            try
            {
                SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
                connectionStringBuilder.DataSource = @"DESKTOP-8AJBFSI\SQLEXPRESS";
                connectionStringBuilder.InitialCatalog = "Chinook";
                connectionStringBuilder.IntegratedSecurity = true;
                connectionStringBuilder.TrustServerCertificate = true;
                return connectionStringBuilder.ConnectionString;
            }
            catch (Exception)
            {
                return "GetConnection is not working properly.";
            }
           
        }
    }
}
