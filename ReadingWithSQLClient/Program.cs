﻿using ReadingWithSQLClient.Models;
using ReadingWithSQLClient.Repositories;
using System;
using System.Collections.Generic;

namespace ReadingWithSQLClient
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();

            // 1. Read all customers in the DB
            //var customers = repository.GetAllCustomers();
            //foreach (var customer in customers)
            //{
            //    Console.WriteLine(customer.ToString());
            //}
            //---------------------------------------------------------
            // 2. Read a customer by ID
            //var customer = repository.GetCustomerById(2);
            //Console.WriteLine(customer.ToString());
            //---------------------------------------------------------
            // 3. Read a customer by name
            //var customer = repository.GetCustomerByName("Kara");
            //Console.WriteLine(customer.ToString());
            //---------------------------------------------------------
            // 4. Return a subset of customers(with limit & offset)
            //var customers = repository.GetLimitCustomers(6, 3);
            //foreach (var customer in customers)
            //{
            //    Console.WriteLine(customer.ToString());
            //}
            //---------------------------------------------------------
            // 5. Add new customer to DB
            //var testCustomer = new Customer()
            //{
            //    FirstName = "Vilyana",
            //    LastName = "Angelova",
            //    Country = "Germany",
            //    PostalCode = "20095",
            //    Phone = "(0)40/1234567",
            //    Email = "test@test.it"
            //};
            //Console.WriteLine(repository.AddNewCustomer(testCustomer));
            //---------------------------------------------------------
            // 6. Update existing customer
            //var updateCustomer = new Customer()
            //{
            //    CustomerId = 60,
            //    FirstName = "Vilyana",
            //    LastName = "Muller",
            //    Country = "Spain",
            //    PostalCode = "07004",
            //    Phone = "987654321",
            //    Email = "new@new.es"
            //};
            //Console.WriteLine(repository.UpdateCustomer(updateCustomer));
            //---------------------------------------------------------
            // 7. Return the number of customers in each country, ordered DESC
            //var countries = repository.GetCountriesWithNumberOfCustomers();
            //foreach (var country in countries)
            //{
            //    Console.WriteLine(country.ToString());
            //}
            //---------------------------------------------------------
            // 8. Return customers who are the highest spenders, ordered DESC
            //var spenders = repository.GetHighestSpenders();
            //foreach (var spender in spenders)
            //{
            //    Console.WriteLine(spender.ToString());
            //}
            //---------------------------------------------------------
            // 9. Return most popular genre for a customer by customer id (in case of a tie, both)
            //var genres = repository.GetMostPopularGenreOfACustomer(12);
            //foreach (var genre in genres)
            //{
            //    Console.WriteLine(genre.ToString());
            //}
        }
    }
}
