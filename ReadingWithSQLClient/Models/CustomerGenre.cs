﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingWithSQLClient.Models
{
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Genre { get; set; }
        public int PlayedCount { get; set; }

        public override string ToString()
        {
            return $"Id: {CustomerId} Name: {FirstName} {LastName}, favourite genre: {Genre}, played: {PlayedCount} times";
        }
    }
}
