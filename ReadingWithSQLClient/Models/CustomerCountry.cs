﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadingWithSQLClient.Models
{
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int CustomerCount { get; set; }

        public override string ToString()
        {
            return $"{Country}: {CustomerCount}";
        }
    }
}
